---
title: 'r.connectivity: A set of GRASS GIS modules for connectivity analysis of ecological networks'
tags:
  - Python
  - graph theory
  - network
  - ecology
  - nature
  - conservation
authors:
  - name: Stefan Blumentrath
    orcid: 0000-0001-6675-1331
    affiliation: "1" # (Multiple affiliations must be quoted)
affiliations:
 - name: Norwegian Institute for Nature Research
   index: 1
date: 03 March 2020
bibliography: paper.bib

---

# Summary

Graph-theory has been characterised as an efficient and useful tool for 
conservation planning (e.g. @Bunn:2000, @Calabrese:2004, @Minor:2008, 
@Zetterberg:2010).

*r.connectivity* is a set of three addons to GRASS GIS 7 @GRASS:7 that are 
intended to make graph-theory more easily available to conservation planning.

*r.connectivity.distance* is the first tool of the toolchain (followed by 
*r.connectivity.network* and *r.connectivity.corridors*).

*r.connectivity.distance* loops through all polygons in an input vector map and 
calculates the cost-distance to all the other polygons within a user-defined 
euclidean distance threshold. It produces two vector maps that hold the network 
information:
 - a vertex-map (with centroid representations of the input patches) and
 - an edge-map (with computed connections between habitat patches).



# Mathematics

Single dollars ($) are required for inline mathematics e.g. $f(x) = e^{\pi/x}$

Double dollars make self-standing equations:

$$\Theta(x) = \left\{\begin{array}{l}
0\textrm{ if } x < 0\cr
1\textrm{ else}
\end{array}\right.$$

You can also use plain \LaTeX for equations
\begin{equation}\label{eq:fourier}
\hat f(\omega) = \int_{-\infty}^{\infty} f(x) e^{i\omega x} dx
\end{equation}
and refer to \autoref{eq:fourier} from text.

# Citations

Citations to entries in paper.bib should be in
[rMarkdown](http://rmarkdown.rstudio.com/authoring_bibliographies_and_citations.html)
format.

If you want to cite a software repository URL (e.g. something on GitHub without a preferred
citation) then you can do it with the example BibTeX entry below for @fidgit.

For a quick reference, the following citation commands can be used:
- @Bunn:2000
- [@Calabrese:2004] -> "(Author et al., 2001)"
- [@Framstad:2012] -> "(Author1 et al., 2001; Author2 et al., 2002)"

# Figures

Figures can be included like this:
![Caption for example figure.\label{fig:example}](figure.png)
and referenced from text using \autoref{fig:example}.

Fenced code blocks are rendered with syntax highlighting:
```python
for n in range(10):
    yield f(n)
```	

# Acknowledgements

The development of the software was funded by internal funds at 
[NINA](www.nina.no) and significant updates were made for the 
[ECOFUNC](www.nina.no) project financed by the Norwegian Research Councile 
Miljøforsk grant XXX.

# References